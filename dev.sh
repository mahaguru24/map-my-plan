#!/bin/bash
echo "Getting API up and running "

# remove -d to run containers in non detach mode
docker-compose up -d
echo "API is available on localhost:8000"

echo "Getting Frontend up and running "

# update frontend dependencies
cd frontend && npm install && npm run dev
ls
docker-compose down
echo "Cya!"