<?php

/**
 * CakePHP 3 StripeComponent
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Http\Client;

/**
 * Atlas Component
 *
 */
class AtlasComponent extends Component {
    
    /**
     *  For saving the reflection class, to use in the loop
     *
     * @var array
     */
    protected static $atlasUrl = 'https://atlas.atdw-online.com.au/api/atlas/';


    /**
     *  For saving the reflection class, to use in the loop
     *
     * @var array
     */
    protected static $atlasAPIKey = '123456789101112';
    /*
    
    */
    protected static function getData( $url = '', $options = [] ) {
        // todo :: find a good way to do it
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');

        $http = new Client();
        $options['key'] = self::$atlasAPIKey;
        $options['st']  = 'NSW';
        $options['out'] = 'json';
        $response = json_encode([ "error" => 'ATLAS API Integration Failed.']);
        try {
            $response = $http->get( self::$atlasUrl . $url , $options);  
            $response = $http
                ->get( self::$atlasUrl . $url , $options);
            $response = iconv('UTF-16LE' , 'UTF-8' ,$response->body()); 
        } catch (Exception $e) {
            
        }
        return $response;
    }

    public static function getAllRegions($options = []) {

        return self::getData('regions', $options );

    }

    public static function getAllAreas($options = []) {

        return self::getData('areas', $options );

    }

    public static function getAllAccommdations($options = []) {
        $options['cats'] = 'ACCOMM';
        return self::getData('products', $options);
    }

}
