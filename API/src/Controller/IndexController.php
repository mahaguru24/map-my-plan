<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class IndexController extends AppController
{

    /**
     * Returns list of regions
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function regions(...$path)
    {
        echo Component\AtlasComponent::getAllRegions([]);
        exit();
    }

    public function areas(...$path)
    {
        
        echo Component\AtlasComponent::getAllAreas([]);
        exit();
    }


    public function accommodations(...$path)
    {   $data = $this->getRequest()->getParam('?');
        if (!empty($data['rg'])) {
            $options['rg'] = $data['rg'];
        }
        if (!empty($data['ar'])) {
            $options['ar'] = $data['ar'];
        }
        if (!empty($data['pge'])) {
            $options['pge'] = $data['pge'];
        }

        $options['pge'] = !empty($data['pge']) ? $data['pge'] : 1;
        $options['size'] = !empty($data['size']) ? $data['pge'] : 12;

        echo Component\AtlasComponent::getAllAccommdations($options);
        exit();
    }

}
