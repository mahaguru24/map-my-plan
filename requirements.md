# PHP Developer Test

## Purpose
The purpose of this test is to determine your proficiency with PHP and your skill
as a full-stack developer.

## Task
Build a single page application that uses data provided from the
[ATDW Atlas API](http://developer.atdw.com.au/ATLAS/API/ATDWO-atlas.html) to list
accommodation options in Sydney. Using the service you develop the frontend
Application shall enable the user to filter results by regions and areas as defined
by the ATDW API.

This is to be build by creating a PHP backend application that consumes the data
from the Atlas API in JSON. The service will then provide the data to the frontend.

The visual result we want is to have a page that uses a frontend framework
(preferably Vue) to render and filter the content.
A minimum of two filters are required:
  * Filter by Region
  * Filter by Area

### Data source
* Data can be called using the methods defined at
[ATDW Atlas API](http://developer.atdw.com.au/ATLAS/API/ATDWO-atlas.html).
All documentation can be found via this link.
* For development the API key is: `123456789101112`

## Requirements
* Responses will be handled in JSON
* Application must be structured using the MVC paradigm.

## What we are looking for
* How you solve the problem in code.
* Code style - Primarily consistency and inline documentation.
* Commit history (This is extremely important. Do not overlook this.)

### Hint
* The ATDW uses UTF-16LE encoding.
* Loading all content and filtering in the frontend is not an option due to the
volume.
* Similar functionality can be seen at the bottom of this
[page](http://int.sydney.com/destinations/sydney).
